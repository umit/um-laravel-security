<?php

namespace UMT\Security;

use Illuminate\Support\Facades\Redirect;
use \Session;
use \Config;

class Security
{
	public static function require_valid_user($unauthorized_path)
	{
		if (!Session::get('user')) {
			Authentication::login();
		}
		if (!Session::get('user')) {
			return \Redirect::to($unauthorized_path);
		}
	}

	public static function require_group($group, $unauthorized_path)
	{
		if (!Session::get('user')) {
			Authentication::login();
		}

		$authorization = new Authorization();
		if (!$authorization->is_authorized($group)) {
			return \Redirect::to($unauthorized_path);
		}
	}

	public static function logout($cas = null)
	{
		if (empty($cas)) {
			\Auth::logout();
			\Session::flush();
			\Session::forget("user");
			\Session::put("user", '');
            Authentication::logout();
		} else {
			Authentication::logout();
		}
	}
}