<?php

namespace UMT\Security;

use \Config;
use \File;
use \View;
use \Less;
use \Response;
use \Route;
use Illuminate\Support\ServiceProvider;

class SecurityServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */

    const default_unauthorized_view = "no-auth";

    public function register()
    {
        $this->app['security'] = $this->app->share(function ($app) {
            return new Security();
        });
    }

    public function boot()
    {
        $this->package('umt/security');
        $this->registerFilters();
        $this->registerRoutes();
    }

    public function registerRoutes()
    {
        Route::get(SecurityServiceProvider::default_unauthorized_view, function () {
            return View::make('security::umt-default-unauthorized');
        });
    }

    public function registerFilters()
    {
        \Route::filter('cas_valid_user', function ($route, $request, $unauthorized = "") {
            $unauthorized = (!empty($unauthorized) ? $unauthorized : SecurityServiceProvider::default_unauthorized_view);
            return Security::require_valid_user($unauthorized);
        });


        \Route::filter('cas_require_group', function ($route, $request, $group, $unauthorized = null) {
            $unauthorized = (!empty($unauthorized) ? $unauthorized : SecurityServiceProvider::default_unauthorized_view);
            return Security::require_group($group, $unauthorized);
        });
    }
}
