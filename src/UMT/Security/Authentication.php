<?php

namespace UMT\Security;

use \Config;
use Whoops\Example\Exception;
use Whoops\Exception\ErrorException;

class Authentication
{

    public function __construct()
    {

    }

    public static function login()
    {
        Authentication::initialize_cas();
        \phpCAS::forceAuthentication();
        $user = \phpCAS::getUser();
        $userAttributes = (object)\phpCAS::getAttributes();


        \Session::put("user", $userAttributes);

        return $user;
    }

    public static function check_login()
    {
        $user = null;
        $authenticated = null;
        Authentication::initialize_cas();
        try {
            $authenticated = \phpCAS::checkAuthentication();
            if ($authenticated) {
                $user = \phpCAS::getUser();
                $userAttributes = (object)\phpCAS::getAttributes();
                \Session::put("user", $userAttributes);

            }
        } catch (\CAS_AuthenticationException $e) {

        }

        return $user;
    }

    private static function initialize_cas()
    {
        $cas_context = Config::get('security.cas_context') ? Config::get('security.cas_context') : Config::get('security::cas_context');
        $cas_host = Config::get('security.cas_host') ? Config::get('security.cas_host') : Config::get('security::cas_host');
        $cas_port = Config::get('security.cas_port') ? Config::get('security.cas_port') : Config::get('security::cas_port');

        \phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);
        \phpCAS::setNoCasServerValidation();
    }

    public static function logout()
    {

        Authentication::initialize_cas();
        \phpCAS::logout();
        return \phpCAS::getServerLogoutURL();

    }
}