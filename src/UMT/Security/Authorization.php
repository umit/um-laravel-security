<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ns159438e
 * Date: 8/13/13
 * Time: 5:37 PM
 * To change this template use File | Settings | File Templates.
 */

namespace UMT\Security;

use \Config;
use \Httpful;

class Authorization
{

	private $user;
	private $password;
	private $uri;

	function __construct()
	{

	}

	/**
	 * Gets the groups for a the logged in user
	 * @return array
	 */
	public function get_user_groups()
	{
		$data = (object)\Session::get("user");


		return isset($data->isMemberOf) ? $data->isMemberOf : array();
	}

	/**
	 * verifies that given user is in the given grouper group
	 * @param string $group
	 * @return boolean
	 */
	public function is_authorized($group)
	{
		$group = $this->parse_group_name($group);
		$is_auth = false;
		$groups = $this->get_user_groups();
        if (in_array($group, $groups)) {
			$is_auth = true;
		}
		return $is_auth;
	}


	private function parse_group_name($group)
	{
		return str_replace("|", ":", $group);
	}

}